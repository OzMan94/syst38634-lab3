package lab3;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test 
	public void testCheckPasswordLengthHappy() {
		boolean result = PasswordValidator.checkPasswordLength("Password10");
		assertTrue("The password isnt suitable", result==true);
	}
	
	@Test
	public void testCheckPasswordLengthException() {
		boolean result = PasswordValidator.checkPasswordLength("t");
		assertTrue("The password isnt suitable", result==false);
	}

	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean result = PasswordValidator.checkPasswordLength("Password");
		assertTrue("The password isnt suitable", result==true);
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean result = PasswordValidator.checkPasswordLength("Pass123");
		assertTrue("The password isnt suitable", result==false);
	}
	
	
	
	
	
	@Test
	public void testCheckNumberOfDigitsHappy() {
		boolean result = PasswordValidator.checkNumberOfDigits("01");
		assertTrue("The password isnt suitable", result==true);
	}
	
	@Test
	public void testCheckNumberOfDigitsException() {
		boolean result = PasswordValidator.checkNumberOfDigits("0");
		assertTrue("The password isnt suitable", result==false);
	}
	
	@Test
	public void testCheckNumberOfDigitsBoundaryIn() {
		boolean result = PasswordValidator.checkNumberOfDigits("11");
		assertTrue("The password isnt suitable", result==true);
	}
	
	@Test
	public void testCheckNumberOfDigitsBoundaryOut() {
		boolean result = PasswordValidator.checkNumberOfDigits("1");
		assertTrue("The password isnt suitable", result==false);
	}
	
}
