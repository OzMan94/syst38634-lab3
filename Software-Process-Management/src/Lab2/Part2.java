package Lab2;

import java.util.Scanner;

public class Part2 {

	public static void main(String args[]) {

		 smallestNumber();
//		 averageNumber();
//		 System.out.println(middle("350"));
//		 numberOfVowels();
//		indexOfChar();
//		codePoint();
//		uniCodeCodePoint();
//		compareStringsLexicographically();
	}

	public static void smallestNumber() {

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter numbers 25");
		int input1 = scan.nextInt();

		System.out.println("Please enter numbers 27");
		int input2 = scan.nextInt();

		System.out.println("Please enter numbers 29");
		int input3 = scan.nextInt();

		if (input1 < input2 || input1 < input3) {
			System.out.println("Samllest number is: " + input1);
		} else if (input2 < input1 || input2 < input3) {
			System.out.println("Samllest number is: " + input2);
		} else if (input3 < input1 || input3 < input2) {
			System.out.println("Samllest number is: " + input3);
		}
		scan.close();
	}

	public static void averageNumber() {

		Scanner scan = new Scanner(System.in);

		System.out.println("Please enter numbers 25");
		int input1 = scan.nextInt();

		System.out.println("Please enter numbers 45");
		int input2 = scan.nextInt();

		System.out.println("Please enter numbers 65");
		int input3 = scan.nextInt();

		double average = (input1 + input2 + input3) / 3;

		System.out.println("The average value is: " + average);

		scan.close();
	}

	public static String middle(String str) {

		int position;
		int length;
		if (str.length() % 2 == 0) {
			position = str.length() / 2 - 1;
			length = 2;
		} else {
			position = str.length() / 2;
			length = 1;
		}
		return str.substring(position, position + length);
	}

	public static void numberOfVowels() {

		int count = 0;
		String sentence = "w3resource";

		for (int i = 0; i < sentence.length(); i++) {
			char ch = sentence.charAt(i);

			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == ' ') {
				count++;
			}
		}
		System.out.println("Number of vowels in the string: " + count);

	}

	public static void indexOfChar() {

		int count = 0;
		String sentence = "Java Exercises!  10";

		for (int i = 0; i < sentence.length(); i++) {
			count++;

			if (count > 10) {
				char ch = sentence.charAt(i);
				System.out.println("The character at position 10 is " + ch);
				break;
			}
		}

	}

	public static void codePoint() {
		String str = "w3rsource.com";
		int ctr = str.codePointCount(0, 13);
		System.out.println("Codepoint count = " + ctr);
	}
	
	public static void uniCodeCodePoint() {
		String str = "w3rsource.com";
		int ctr = str.codePointCount(0, 9);
		int val = str.codePointAt(8);
		System.out.println("Character(unicode point) at " + ctr + " = " + val );
	}
	
	public static void compareStringsLexicographically() {
		
		String str1 = "This is Exercise 1";
		String str2 = "This is Exercise 2";
		
		int returnVal = str1.compareTo(str2);
		
		if(returnVal == 0) {
			System.out.println('"' +str1 + '"' + " is equal to " + '"' + str2 + '"');
		}
		else if (returnVal < 0) {
			System.out.println('"' +str1 + '"' + " is less than " + '"' + str2 + '"');
		}
		
		
	}
}
