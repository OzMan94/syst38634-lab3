package Time;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TimeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	
	@Test//Happy (Passing) Path
	public void testGetMilliseconds() {
		int milli= Time.getMilliseconds("12:05:05:05");
		assertTrue("The milliseconds were not", milli == 5);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetMillisecondsException() {
		int milli = Time.getMilliseconds("12:05:05:0054");
		assertTrue("The milliseconds were not", milli == 5);
	}
	
	@Test //Boundary In
	public void testGetMillisecondsBoundaryIn() {
		int milli = Time.getMilliseconds("12:05:05:999");
		assertTrue("The milliseconds were not", milli == 999);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testGetMillisecondsBoundaryOut() {
		int milli = Time.getMilliseconds("12:05:05:1000");
		assertTrue("The milliseconds were not", milli == 0);
	}

	
	
	
	@Test
	public void testGetTotalSeconds() {
		int seconds = Time.getTotalSeconds("12:05:05");
		assertTrue("The seconds were not calculated properly", seconds == 43505);
	}

	@Test
	public void testGetSeconds() {
		int seconds = Time.getTotalSeconds("12:05:05");
		assertTrue("The seconds were not calculated properly", seconds == 5);
	}
	

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}

}
